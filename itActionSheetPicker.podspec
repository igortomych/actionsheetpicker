Pod::Spec.new do |s|
  s.name          = "itActionSheetPicker"
  s.version       = "1.0.5"
  s.summary    = "Quickly reproduce the dropdown UIPickerView / ActionSheet functionality from Safari on iPhone/ iOS / CocoaTouch."
  s.homepage   = "https://bitbucket.org/igortomych/actionsheetpicker"
  s.license       = 'BSD'
  s.platform       = :ios
  s.source_files  = 'ActionSheetPicker.h', 'Pickers/*.{h,m}'
  s.public_header_files = 'ActionSheetPicker.h', 'Pickers/*.{h,m}'
  s.framework   = 'UIKit'
  s.requires_arc = true
end